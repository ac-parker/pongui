﻿# Change Log
All notable changes to this project will be documented in this file.
 
The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).
 
## [1.0.1] - 2019-07-28
 
### Added
- Screenshots to `repo-meta` folder for use in the project readme.
 
## [1.0.0] - 2019-07-28
 
### Added
- Initial release of Pongui.
- Ball and paddle objects with physics.
- Main menu with game mode options.
- Score keeping system.
- Event management for game events and UI management.
