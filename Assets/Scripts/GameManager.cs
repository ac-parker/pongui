﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    public Ball ball;
    public Paddle paddle;
    public GameObject gameArea;
    public bool hasAIPlayer;
    public float readyTime = 3f;

    public int winScore = 1;
    private int scoreLeft;
    private int scoreRight;

    public static Vector2 bottomLeft;
    public static Vector2 topRight;

    bool isPlaying = false;
    bool gameEnded = false;
    float timer = 0;

    public delegate void ReadyActive(bool active);
    public static event ReadyActive SetReady;

    public delegate void UpdateScores(string left, string right);
    public static event UpdateScores SetScores;

    public delegate void PlayerWins(int playerIndex);
    public static event PlayerWins SetWinner;

    void Start ()
    {
        Renderer areaRenderer = gameArea.GetComponent<Renderer>();
        bottomLeft = areaRenderer.bounds.min;
        topRight = areaRenderer.bounds.max;

        Ball.PointScored += EndRound;

        scoreLeft = 0;
        scoreRight = 0;

        CreatePaddles();
        SetReady(true);
    }

    void Update()
    {
        if (!isPlaying && !gameEnded)
        {
            timer += Time.deltaTime;
        }

        if (timer >= readyTime && !gameEnded)
        {
            StartRound ();
        }
    }

    private void OnDisable()
    {
        Ball.PointScored -= EndRound;
    }

    void CreatePaddles ()
    {
        Paddle paddle1 = Instantiate(paddle) as Paddle;
        Paddle paddle2 = Instantiate(paddle) as Paddle;

        if (hasAIPlayer)
        {
            paddle1.Init(true, true);
            paddle2.Init(false, false);
        }
        else
        {
            paddle1.Init(true, false);
            paddle2.Init(false, false);
        }
    }

    void StartRound ()
    {
        timer = 0;
        Instantiate(ball);
        isPlaying = true;
        SetReady(false);
    }

    public void EndRound (int left, int right)
    {
        scoreLeft += left;
        scoreRight += right;
        SetScores(left.ToString(), right.ToString());

        if (scoreLeft == winScore || scoreRight == winScore)
        {
            if (scoreLeft > scoreRight)
            {
                SetWinner(0);
            }
            else
            {
                SetWinner(1);
            }

            gameEnded = true;
        }
        else
        {
            isPlaying = false;
            SetReady(true);
        }
    }
}
