﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    [SerializeField]
    float speed = 5f;

    [SerializeField]
    float maxSpeed = 15f;

    float radius;
    Vector2 direction;

    public delegate void PlayerScores(int left, int right);
    public static event PlayerScores PointScored;

    void Start()
    {
        // The start direction is random towards the left paddle.
        direction = new Vector2(-1, UnityEngine.Random.Range(-0.3f, 0.3f)).normalized;
        radius = transform.localScale.x / 2;
    }

    void Update()
    {
        transform.Translate(direction * Time.deltaTime * speed);

        // If we're at the top or bottom of the area, flip our y direction.
        if (transform.position.y < GameManager.bottomLeft.y + radius && direction.y < 0)
        {
            direction.y = -1 * direction.y;
        }

        if (transform.position.y > GameManager.topRight.y - radius && direction.y > 0)
        {
            direction.y = -1 * direction.y;
        }

        if (transform.position.x < GameManager.bottomLeft.x + radius && direction.x < 0)
        {
            PointScored(0, 1);
            Destroy(gameObject);
        }

        if (transform.position.x > GameManager.topRight.x - radius && direction.x > 0)
        {
            PointScored(1, 0);
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Paddle")
        {
            direction.x = -1 * direction.x;

            // Calculate a bounce angle between 45 and -45 degrees, based on where on the paddle the ball was hit.
            float ballY = transform.position.y - other.bounds.min.y;
            float paddleHeight = other.bounds.max.y - other.bounds.min.y;
            float collisionHeight = ballY / paddleHeight;

            float bearing = -90 * collisionHeight + 135;
            double bearingRadians = bearing * Math.PI / 180;
            Vector2 rotationVector = new Vector2((float)Math.Sin(bearingRadians), (float)Math.Cos(bearingRadians));

            // If we hit the right paddle, flip our direction.
            if(direction.x < 0)
            {
                rotationVector.x *= -1;
            }

            direction = rotationVector.normalized;

            // Increase speed with each hit for fun!
            if (speed <= maxSpeed)
            {
                speed += 0.3f;
            }
        }
    }
}
