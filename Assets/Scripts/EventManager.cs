﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class EventManager : MonoBehaviour
{
    public GameObject scoreTextLeft;
    public GameObject scoreTextRight;
    public GameObject readyPanel;
    public GameObject endPanel;
    public TextMeshProUGUI winText;

    private TextMeshProUGUI tmpLeft;
    private TextMeshProUGUI tmpRight;

    void Start()
    {
        tmpLeft = scoreTextLeft.GetComponent<TextMeshProUGUI>();
        tmpRight = scoreTextRight.GetComponent<TextMeshProUGUI>();

        GameManager.SetReady += SetReady;
        GameManager.SetScores += SetScores;
        GameManager.SetWinner += SetWinner;
    }

    public void SetScores (string left, string right)
    {
        tmpLeft.SetText(left);
        tmpRight.SetText(right);
    }

    public void SetReady (bool active)
    {
        readyPanel.SetActive(active);
    }

    public void SetWinner (int playerIndex)
    {
        endPanel.SetActive(true);

        if (playerIndex == 0)
        {
            winText.SetText("LEFT WINS!");
        }
        else
        {
            winText.SetText("RIGHT WINS!");
        }
    }

    private void OnDisable()
    {
        GameManager.SetReady -= SetReady;
        GameManager.SetScores -= SetScores;
        GameManager.SetWinner -= SetWinner;
    }
}
