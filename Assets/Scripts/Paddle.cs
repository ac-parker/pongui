﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour
{
    // Make the player and AI speeds editable in the inspector.
    [SerializeField]
    float speed = 8f;

    // AI speed is set as a ratio to the player speed.
    [SerializeField]
    float speedAI = 0.3f;

    float height;
    string input;
    bool isRight;
    bool isAI;

    void Start ()
    {
        height = transform.localScale.y;
    }

    public void Init(bool isRightPaddle, bool isAIPaddle)
    {
        isRight = isRightPaddle;
        isAI = isAIPaddle;

        Vector2 startPosition = Vector2.zero;

        // Set start positions for each paddle.
        if (isRightPaddle)
        {
            startPosition = new Vector2(GameManager.topRight.x, 0);
            startPosition += Vector2.left * transform.localScale.x;

            // Flip the right paddle to ensure the correct side has collisions.
            transform.Rotate(new Vector3(0, 180, 0));

            input = "PaddleRight";
        }
        else
        {
            startPosition = new Vector2(GameManager.bottomLeft.x, 0);
            startPosition += Vector2.right * transform.localScale.x;

            input = "PaddleLeft";
        }

        transform.position = startPosition;
        transform.name = input;
    }

    void Update ()
    {
        float direction = 0;

        // If the paddle is AI controlled, track the ball and move accordingly.
        if (isAI)
        {
            GameObject[] balls = GameObject.FindGameObjectsWithTag("Ball");
            if (balls.Length > 0)
            {
                GameObject ball = balls[0];
                Collider2D paddleCollider = GetComponent<Collider2D>();

                // The AI attempts to keep the ball in line with the centre of its paddle.
                if (ball.transform.position.y < (paddleCollider.bounds.min.y + height/4))
                {
                    direction = -speedAI;
                }
                else if (ball.transform.position.y > (paddleCollider.bounds.max.y - height/4))
                {
                    direction = speedAI;
                }
            }
        }

        // If the paddle is player controlled, get the player input and move accordingly.
        else

        {
            direction = Input.GetAxis(input);
        }

        if (transform.position.y < GameManager.bottomLeft.y + height / 2 && direction < 0)
        {
            direction = 0;
        }

        if (transform.position.y > GameManager.topRight.y - height / 2 && direction > 0)
        {
            direction = 0;
        }

        float move = direction * Time.deltaTime * speed;
        transform.Translate(move * Vector2.up);
    }
}
