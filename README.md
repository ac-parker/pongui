﻿# pongui

A pong clone built as a learning exercise, focusing on using Unity's UI components. Based on [Tanay Singhal's tutorial](https://www.youtube.com/watch?v=1oY--Zk9b6w).

Uses [aaaleee's scanline shader](https://github.com/aaaleee/UnityScanlinesEffect).

## Screenshots

![Game's main menu with logo and buttons for launching the game](./repo-meta/pongui_1.PNG)
![Pongui gameplay with two players](./repo-meta/pongui_2.png)
![The screen shown after a player wins a game](./repo-meta/pongui_3.PNG)